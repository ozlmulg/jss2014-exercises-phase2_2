package tr.com.obss.jss2014.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SessionServlet")
public class SessionServlet extends HttpServlet {
	private static final String START_DATE = "start_date";
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute(START_DATE)==null){
			session.setAttribute(START_DATE, new Date());
		}
		Object sessionStartDate = session.getAttribute(START_DATE);
		StringBuilder sb = new StringBuilder();
		sb.append("Current Time: ").append(new Date()).append("\n");
		sb.append("Session Time: ").append(sessionStartDate);
		response.getWriter().write(sb.toString());
	}



}
