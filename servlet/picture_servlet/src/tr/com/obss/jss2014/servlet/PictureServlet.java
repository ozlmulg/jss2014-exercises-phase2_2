package tr.com.obss.jss2014.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PictureServlet
 */
@WebServlet("/PictureServlet")
public class PictureServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static final String PICTURES_HOME = "/home/yasa/wallpapers/";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String filename = request.getParameter("filename");
		if(filename==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("filename parameter is required");
			return;
		}
		File file = new File(PICTURES_HOME+filename);
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.getWriter().write("file not found");
			return;
		}
		String fileType = Files.probeContentType(file.toPath());
		if(fileType==null){
			inputStream.close();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().write("file is of bad type");
			return;
		}
		response.setContentType(fileType);
		copy(inputStream, response.getOutputStream());
		inputStream.close();
	}
	
	public static long copy(InputStream from, OutputStream to)
		      throws IOException {
		    byte[] buf = new byte[1024];
		    long total = 0;
		    while (true) {
		      int r = from.read(buf);
		      if (r == -1) {
		        break;
		      }
		      to.write(buf, 0, r);
		      total += r;
		    }
		    return total;
		  }



}
