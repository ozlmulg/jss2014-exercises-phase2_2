<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href="./css/jumbotron-narrow.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP MVC Example</title>
</head>
<body>
<div class="container">
      <div class="header">
        <h3 class="text-muted">JSP MVC Example</h3>
      </div>

      <div class="jumbotron">
        <h1>JSP MVC Example</h1>
        <p class="lead">The button below keeps track of the last time it was pressed!</p>

        <p><a class="btn btn-lg btn-success" href="?button=true" role="button">Click Me!</a></p>
      </div>

      <div class="row marketing">
        <div class="col-lg-6">
          <h4>Current Time</h4>
          <p>${ currentTime }</p>

          <h4>Last Button Click Time</h4>
          <p>${ buttonTime }</p>

          <h4>Session Start Time</h4>
          <p>${ sessionTime }</p>
          
          <h4>Random Name</h4>
          <p>${ nameBean.randomName }</p>
        </div>
      </div>

      <div class="footer">
        <p>&copy; OBSS 2014</p>
      </div>

    </div> <!-- /container -->
</body>
</html>