package tr.obss.jss2014.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DispatcherServlet
 */
@WebServlet("/example")
public class DispatcherServlet extends HttpServlet {

	private static final String BUTTON_DATE = "buttonDate";
	private static final String START_DATE = "start_date";
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		if("true".equals(request.getParameter("button"))){
			session.setAttribute(BUTTON_DATE, new Date());
			response.sendRedirect(request.getContextPath()+request.getServletPath());
			return;
		}
		
		if(session.getAttribute(START_DATE)==null){
			session.setAttribute(START_DATE, new Date());
		}
		
		Object sessionStartDate = session.getAttribute(START_DATE);
		
		Object buttonTime = session.getAttribute(BUTTON_DATE);
		String buttonTimeString = buttonTime==null?"Never":buttonTime.toString();
		request.setAttribute("currentTime", new Date());
		request.setAttribute("sessionTime", sessionStartDate);
		request.setAttribute("buttonTime", buttonTimeString);
		request.getRequestDispatcher("/WEB-INF/page1.jsp").forward(request, response);
	}

}
